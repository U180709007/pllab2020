//#ifndef HIGHPRECISION_H_INCLUDED
//#define HIGHPRECISION_H_INCLUDED


typedef struct{

int digits[20];
int decpt;
int sign;

}high_prec_t;


extern int scanHighPrec(high_prec_t* h);

extern void printHighPrec(high_prec_t h);
